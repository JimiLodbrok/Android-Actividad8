package com.example.jaime.actividad3.utils.cells;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.jaime.actividad3.R;

public class CeldaJugador extends RecyclerView.ViewHolder {
    private TextView nombreJugador;
    private TextView equipoJugador;
    private TextView alturaJugador;
    private ImageView imagenJugador;

    public CeldaJugador(View itemView) {
        super(itemView);
        nombreJugador = itemView.findViewById(R.id.nombreJugador);
        equipoJugador = itemView.findViewById(R.id.equipoJugador);
        alturaJugador = itemView.findViewById(R.id.alturaJugador);
        imagenJugador = itemView.findViewById(R.id.imagenJugador);
    }

    public void setNombreJugador(String nombre) {
        this.nombreJugador.setText(nombre);
    }

    public void setNombreEquipo(String equipo) {
        this.equipoJugador.setText(equipo);
    }

    public ImageView getImagenJugador() {
        return imagenJugador;
    }

    public void setAlturaJugador(String nombre) {
        this.alturaJugador.setText(nombre);
    }
}
