package com.example.jaime.actividad3.model.firebase;

import com.example.jaime.actividad3.model.persistence.Jugador;

public interface FirebaseDataListener {
    void dataLoaded();
    void addMarkers(int contador);
    void removeMarkers();
    void addDataToSQL(Jugador jugador,String id);
}
