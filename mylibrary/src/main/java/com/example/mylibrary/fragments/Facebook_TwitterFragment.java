package com.example.mylibrary.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mylibrary.R;
import com.example.mylibrary.events.EventsAdmin;

/**
 * A simple {@link Fragment} subclass.
 */
public class Facebook_TwitterFragment extends Fragment {
    private TextView nombre,amigos,amigoss;
    private Button btnCerrar;
    private ImageView imagenPerfil;

    public Facebook_TwitterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_facebook, container, false);
        nombre = v.findViewById(R.id.nombre);
        amigos = v.findViewById(R.id.amigos);
        amigoss = v.findViewById(R.id.amigoss);
        btnCerrar = v.findViewById(R.id.btnCerrar);
        imagenPerfil = v.findViewById(R.id.fotoPerfil);
        btnCerrar.setOnClickListener(EventsAdmin.getInstance());
        return v;
    }

    public TextView getNombre() {
        return nombre;
    }

    public void setNombre(TextView nombre) {
        this.nombre = nombre;
    }

    public TextView getAmigos() {
        return amigos;
    }

    public void setAmigos(TextView amigos) {
        this.amigos = amigos;
    }

    public TextView getAmigoss() {
        return amigoss;
    }

    public void setAmigoss(TextView amigoss) {
        this.amigoss = amigoss;
    }

    public ImageView getImagenPerfil() {
        return imagenPerfil;
    }

    public void setImagenPerfil(ImageView imagenPerfil) {
        this.imagenPerfil = imagenPerfil;
    }
}
